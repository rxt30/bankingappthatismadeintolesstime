//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//--Imports-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
package sample.CustomerChange;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import sample.Login.Login;
import sample.Main;
import java.io.IOException;

import sample.MenuBarController.MenuBarController; // NEW ONE
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//--Set all FX-Items----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
public class CustomerChangeController extends MenuBarController{
    public GridPane registerPane;
    public Label mailErrorLabel;
    public Label passwordMatchLabel;
    public Button changeButton;
    public TextField passwordField;
    public TextField repeatPasswordField;
    public TextField mailField;
    public TextField lastNameField;
    public TextField firstNameField;
    public TextField adressField;
    public TextField cityField;
    public TextField mobileNumberField;
    public DatePicker birthDatePicker;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//---CHANGE Customer----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Button for changing the entered (new) user data
     * changeButton() gets the status (0 or 1) from the change method in CustomerChange.java
     * @param event Triggered Event
     */
    @FXML //Button for change the new given details 
    private void changeButton(ActionEvent event){
        event.consume();
        int status=CustomerChange.changeCustomer(firstNameField.getText(), lastNameField.getText(), adressField.getText(), cityField.getText(), mailField.getText(), mobileNumberField.getText(), birthDatePicker.getValue().toString(), passwordField.getText());
        if(status==0){
            System.out.println("Account details changed");
            //changed
        }
        else if(status==1){
            System.out.println("There is an error");
            Main.showErrorWindow();
            //not changed --> error
        }

    } 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//---DELETE Customer----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Button for deleting user data
     * deleteButton() gets the status (0 or 1) from the delete method in CustomerChange.java
     * @param event Triggered Event
     */
@FXML //Button for delte the customer-account
    private void deleteButton(ActionEvent event){
        event.consume();
        int status = CustomerChange.deleteCustomer();
        if(status==0){
            System.out.println("Account deleted!");
            //deleted
        }
        else if(status==1){
            System.out.println("There is an error");
            Main.showErrorWindow();
            //not changed --> error
        }
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//----same as customer creation-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//These are the same methods as in the customercreation-file to check if all of the fields are ok
//For detailed documentation go to CustomerCreation/Controller.java
    @FXML
    private void backToLogin(ActionEvent event) {
        event.consume();
        try {
            Parent loginWindow = FXMLLoader.load(Login.class.getResource("login.fxml"));
            Main.getPrimaryStage().setScene(new Scene(loginWindow,300,300));
        } catch(IOException error){
            System.out.println("A error occured");
            Main.showErrorWindow();
        }
    }

    @FXML
    private void checkInputFields(Event event) {
        event.consume();
        for (int i = 1; i < 16; i += 2) {
            if (((TextField) registerPane.getChildren().get(i)).getText().equals("") || ((TextField) registerPane.getChildren().get(i)).getText() == null) {
                changeButton.setDisable(true);
                return;
            }
        }
        if (birthDatePicker.getValue() == null || birthDatePicker.getValue().toString().equals("")) {
            return;
        }
        if (checkPasswordFields()) {
            changeButton.setDisable(false);
        }
    }


    private boolean checkPasswordFields(){
            if(passwordField.getText().equals("") || repeatPasswordField.getText().equals("")){
                return false;
            }
            if(passwordField.getText().equals(repeatPasswordField.getText())){
               passwordMatchLabel.setPrefHeight(0);
                return true;
            }
            passwordMatchLabel.setPrefHeight(16);
            return false;
    }
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
}