//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//--Imports-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
package sample.CustomerChange;
import java.sql.Connection;
import java.sql.*;
import sample.*;
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//--Get the ID (-->later)-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
public class CustomerChange {
    private static int id;

    public int getId(){return id;}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//--Connect to SQL and change the data----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Change the user data
     * @param firstName First Name of the customer
     * @param lastName Last Name of the customer
     * @param adress Address of the customer
     * @param city City of the customers home
     * @param mail Customers mail
     * @param mobileNumber Customers Phone Number
     * @param birthday Customers birthday
     * @param password Customers account password
     * @return 1 if there was an error while changing
     */
    public static int changeCustomer(String firstName, String lastName, String adress, String city, String mail, String mobileNumber, String birthday, String password){
        try {
            PreparedStatement setChange = Main.getMariadbConnection().prepareStatement("UPDATE customer SET firstName = ?, lastName =?, adress=?, city=?, mail=?, mobileNumber=?, birthday=?, password = ? WHERE id =?");
            //Copy the new user data into the PreparedStatement
            setChange.setString(1,firstName);
            setChange.setString(2,lastName);
            setChange.setString(3,adress);
            setChange.setString(4,city);
            setChange.setString(5,mail);
            setChange.setString(6,mobileNumber);
            setChange.setString(7,birthday);
            setChange.setString(8,password);
            setChange.setInt(9,Main.getUserAccount().getId()); //Get the ID of the customer who wants to change the data
            ResultSet queryResult = setChange.executeQuery();
            if(!queryResult.next()){
                //no customer to change there
            }
        } catch (SQLException error) {
            error.printStackTrace();
            System.out.println("You should look for an error!"); //Data couldn't be changed
            Main.showErrorWindow();
            return 1; //return different numbers for if-statement in CustomerChangeController
        }
        return 0;
    }
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


//--Connect to SQL and DELTE the data----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    /**
     * Delete the user data
     * @return 1 if there was an error while deleting
     */
    public static int deleteCustomer(){
        try {
            PreparedStatement set_change = Main.getMariadbConnection().prepareStatement("DELETE FROM customer WHERE id= ?");
            set_change.setInt(1, Main.getUserAccount().getId()); //Get the ID of the customer that will be deleted
            set_change.executeUpdate(); 
        } catch (SQLException error) {
            error.printStackTrace();
            System.out.println("You should look for an error!");
            Main.showErrorWindow();
            return 1;
        }
        return 0;
    }

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------



//--OLD-Version: Leave the old fillCustomer for later use-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//        public void fillCustomer(ResultSet queryResult){
//        try {
//            id = queryResult.getInt("id");
//            String firstName = queryResult.getString("firstName");
//            String lastName = queryResult.getString("lastName");
//            String adress = queryResult.getString("adress");
//            String city = queryResult.getString("city");
//            String mail = queryResult.getString("mail");
//            String birthday = queryResult.getString("birthday");
//            String mobileNumber = queryResult.getString("mobileNumber");
//            int isAdmin = queryResult.getInt("isAdmin");
//        }
//        catch(SQLException error){
//            System.out.println("A error occured");
//            error.printStackTrace();
//        }
//    }
}
//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
