package sample.MainWindow;

import sample.Main;

import java.sql.ResultSet;

/**
 * Class used for the objects in the table at mainWindow.fxml
 */
public class UserAccount {
    private String iban;
    private int kontoType;
    private double currentBalance;

    public UserAccount(ResultSet data){
        try {
            iban = data.getString("iban");
            kontoType = data.getInt("kontoType");
            currentBalance = data.getDouble("currentBalance");
        }
        catch(Exception error){
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }

    public String getIban() {
        return iban;
    }

    public String getKontoType() {
        if(kontoType == 1){
            return "Girokonto";
        }else{
            return "Debitkonto";
        }
    }

    public double getCurrentBalance() {
        return currentBalance;
    }
}
