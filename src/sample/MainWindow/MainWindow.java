package sample.MainWindow;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import sample.Main;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Generation class of the table in mainWindow.fxml
 */
public class MainWindow {

    /**
     * Generates the given Table with all the Accounts from the current User
     * @param accountsTable Table to fill
     */
    public static void generateAccountsTable(TableView accountsTable){
        accountsTable.getColumns().clear();
        accountsTable.getItems().clear();
        ResultSet userAccounts = getUsersAccounts();
        makeTableHeader(accountsTable,userAccounts);
        makeTableBody(accountsTable,userAccounts);
        accountsTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
    }

    /**
     * Generates the Header for the given Table with the given ResultSet
     * NOTE: Only usuable when filling the table with Objects of the Class UserAccount
     * @param accountTable Table to fill
     * @param data Data to make the Header from
     */
    private static void makeTableHeader(TableView accountTable, ResultSet data){
        TableColumn currentColumn;
        String currentColumnName;
        try {
            for (int i = 1; i <= data.getMetaData().getColumnCount(); i++) {
                currentColumnName = data.getMetaData().getColumnName(i);
                currentColumn = new TableColumn(currentColumnName);
                currentColumn.setCellValueFactory(new PropertyValueFactory<>(currentColumnName));
                accountTable.getColumns().add(currentColumn);
            }
        }
        catch(Exception error){
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }

    /**
     * Fills the table with the given Data.
     * NOTE: Only usuable when filling the table with Objects of the Class UserAccount
     * @param accountTable Table to fill
     * @param data Data to be fill the Table with
     */
    private static void makeTableBody(TableView accountTable,ResultSet data){
        try {
            data.beforeFirst();
            while(data.next()){
                accountTable.getItems().add(new UserAccount(data));
            }
        }
        catch(Exception error){
            error.printStackTrace();
            Main.showErrorWindow();
        }

    }

    /**
     * Gets all the Accounts for the current User
     * @return ResultSet with all Accounts. Columns: (iban,currentBalance,kontoType)
     */
    private static ResultSet getUsersAccounts(){
        String selectQuery = "SELECT iban,currentBalance,kontoType from account where customerId = ?";
        try{
            PreparedStatement usersAccountsQuery = Main.getMariadbConnection().prepareStatement(selectQuery);
            usersAccountsQuery.setInt(1,Main.getUserAccount().getId());
            return usersAccountsQuery.executeQuery();
        }
        catch(Exception error){
            error.printStackTrace();
            Main.showErrorWindow();
            return null;
        }
    }
}
