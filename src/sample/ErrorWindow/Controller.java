package sample.ErrorWindow;

import javafx.fxml.FXML;

/**
 * Controller for ErrorWindow.fxml
 */
public class Controller {
    /**
     * Closes the Program
     */
    @FXML
    private void shutdownProgram(){
        System.exit(0);
    }
}
