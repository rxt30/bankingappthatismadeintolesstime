package sample;
import org.junit.*;

import sample.Account.Account;

import static junit.framework.TestCase.*;

public class TestUnit {
    Account testAccount = new Account();
    String falseIban = "DE40420066600000000009";
    String trueIban = "DE40420066600000000008";


    public void testIban(){
        assertTrue(testAccount.checkIBAN(trueIban));
        assertFalse(testAccount.checkIBAN(falseIban));
        assertEquals("DE42795500000012127361",Account.createIBAN(79550000,12127361));
    }

}
