package sample.Account.createAccount;

import sample.Account.Account;
import sample.Main;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The class createAccount is a backend class amd creates new Accounts if the methods createdebitAccount or creategiroAccount are run.
 */
public class createAccount {

    /**
     * The function creategiroAccount runs the function create Account with the value 1 for kontoType
     * @param customerId
     */
    //Method for creating a giro Account
    public static void creategiroAccount(int customerId){
        createAccount(customerId,1);
    }

    /**
     * The function createdebitAccount runs the function create Account with the value 2 for kontoType
     * @param customerId
     */
    //Method for creating a debit Account
    public static void createdebitAccount(int customerId){
        createAccount(customerId,2);
    }


    /**
     * The function createAccount is set to private
     * When the function gets run it creates a new Account in the account table. For this it first creates a new entry with alle right values and a placeholder iban.
     * After that the method searches the accountId for the created account and calls the function create IBAN from the class Account to generate the fitting IBAN.
     * Now the sql entry gets updated with the corrected IBAN.
     * @param customerId
     * @param kontoType
     */
    //Method for creating new Accounts
    private static void createAccount(int customerId, int kontoType){
        double balance=0;
        int blz=42006660;
        String bic="Test";
        String IBAN="Test";

        //Create new entry in account table
        try {
            PreparedStatement createAccount = Main.getMariadbConnection().prepareStatement("INSERT INTO account(customerId, bic, iban, blz, kontoType, currentBalance) VALUES (?,?,?,?,?,?)");
            createAccount.setInt(1, customerId);
            createAccount.setString(2,bic);
            createAccount.setString(3,IBAN);
            createAccount.setInt(4,blz);
            createAccount.setInt(5,kontoType);
            createAccount.setDouble(6,balance);
            createAccount.execute();
            createAccount.close();
        } catch (SQLException throwables) {
            System.out.println("An error occured");
            throwables.printStackTrace();
            Main.showErrorWindow();
        }

        //Get accountId
        int accountId =0;
        try {
            PreparedStatement kontonr = Main.getMariadbConnection().prepareStatement("SELECT * FROM account WHERE iban=?");
            kontonr.setString(1,IBAN);
            ResultSet queryResult = kontonr.executeQuery();
            if(queryResult.next()){
                queryResult.first();
                accountId = queryResult.getInt("accountId");
                queryResult.close();
            }else{
                System.out.println("An error occured");
                Main.showErrorWindow();
            }
        }
        catch(SQLException error){
            System.out.println("An error occured");
            error.printStackTrace();
            Main.showErrorWindow();
            return;
        }

        //Create Iban_reciever
        IBAN=Account.createIBAN(blz, accountId);

        //Update Iban_reciever
        try {
            PreparedStatement updateAccount = Main.getMariadbConnection().prepareStatement("UPDATE account SET iban=? WHERE accountId=?");
            updateAccount.setString(1, IBAN);
            updateAccount.setInt(2, accountId);
            updateAccount.execute();
            updateAccount.close();
        } catch (SQLException throwables) {
            System.out.println("An error occured");
            throwables.printStackTrace();
            Main.showErrorWindow();
        }

    }
}
