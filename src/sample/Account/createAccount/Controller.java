package sample.Account.createAccount;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.text.Text;
import sample.Account.Account;
import sample.Customer.Customer;
import sample.Login.Login;
import sample.Main;
import sample.MenuBarController.MenuBarController;

import java.io.IOException;

/**
 * Class Controller is the Controller for the window createAccount.fxml file.
 * If one of the two buttons in the Window gets pressed it runs the corresponding function.
 */
public class Controller extends MenuBarController {

    /**
     * The method createGiroAccount gets run, when the button "Girokonto" got pressed.
     * Then it runs the method creategiroAccount in the class createAccount.
     * After that the method transfers the user into the createSuccessful window.
     * @param event
     */
    @FXML
    private void createGiroAccount(ActionEvent event){
        int customerid= Main.getUserAccount().getId();


        //giro is picked
        createAccount.creategiroAccount(customerid);
        try {
            Parent mainWindowRoot;
            mainWindowRoot = FXMLLoader.load(createAccount.class.getResource("createSuccessful.fxml"));
            Main.getPrimaryStage().setScene(new Scene(mainWindowRoot));
            Main.getPrimaryStage().show();
        } catch (IOException e) {
            System.out.println("An error occured");
            e.printStackTrace();
            Main.showErrorWindow();
        }
    }

    /**
     * The method createDebitAccount gets run, when the button "Debitkonto" got pressed.
     * Then it runs the method createdebitAccount in the class createAccount.
     * After that the method transfers the user into the createSuccessful window.
     * @param event
     */
    @FXML
    private void createDebitAccount(ActionEvent event){
        int customerid= Main.getUserAccount().getId();

        //debit is picked
        createAccount.createdebitAccount(customerid);

        try {
            Parent mainWindowRoot;
            mainWindowRoot = FXMLLoader.load(createAccount.class.getResource("createSuccessful.fxml"));
            Main.getPrimaryStage().setScene(new Scene(mainWindowRoot));
            Main.getPrimaryStage().show();
        } catch (IOException e) {
            System.out.println("An error occured");
            e.printStackTrace();
            Main.showErrorWindow();
        }
    }
}

