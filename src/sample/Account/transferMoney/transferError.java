package sample.Account.transferMoney;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import sample.Login.Login;
import sample.Main;
import sample.MainWindow.MainWindow;

import java.io.IOException;

/**
 * The class transferError is the controller for the window "transferError".
 * The window transferError shows the user that during the transfer an error occurred.
 */
public class transferError {

    /**
     * As soon as the button "Back to Menu" gets pressed this method gets started and takes the user back to the menu
     * @param event
     */
    @FXML
    private void loadMenu(ActionEvent event){
        try {
            Parent mainWindowRoot;
            mainWindowRoot = FXMLLoader.load(MainWindow.class.getResource("mainWindow.fxml"));
            ((Label) mainWindowRoot.getChildrenUnmodifiable().get(1)).setText("Welcome " + Main.getUserAccount().getFirstName() + " " + Main.getUserAccount().getLastName());
            TableView accountsTable = (TableView) mainWindowRoot.getChildrenUnmodifiable().get(2);
            MainWindow.generateAccountsTable(accountsTable);
            Main.getPrimaryStage().setScene(new Scene(mainWindowRoot));
            Main.getPrimaryStage().show();
        } catch (IOException e) {
            System.out.println("An error occured");
            e.printStackTrace();
            Main.showErrorWindow();
        }
    }
}
