package sample.Account.transferMoney;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import sample.Account.Account;
import sample.Login.Login;
import sample.Main;
import sample.MenuBarController.MenuBarController;

import java.io.IOException;
/**
 * The class Controller is the controller for the window "transferMoney"
 * In this window the user fills in the IBAN for his account, as well as the IBAN of the recipient, the purpose and the amount of the transfer.
 */

public class Controller extends MenuBarController{
    public TextField Iban_sender;
    public TextField Iban_reciever;
    public TextField purpose;
    public TextField amount;
    public Text error_Iban_sender;
    public Text error_Iban_reciever;
    public Text error_amount;

    /**
     * The function testIBAN gets run, when an action occurs in the field iban.
     * It checks for the correct input and sets the visibility for the error_iban_reciever text
     * @param event
     */
    @FXML
    private void testIBAN(ActionEvent event){
        event.consume();
        if(!Account.checkIBAN(Iban_reciever.getText())){
            error_Iban_reciever.setVisible(true);
        }else{
            error_Iban_reciever.setVisible(false);
        }
    }

    /**
     * The function testAmount gets run, when an action occurs in the field amount.
     * It checks for the correct input and sets the visibility for the error_amount text
     * @param event
     */
    @FXML
    private void testAmount(ActionEvent event){
        event.consume();
        double amount_in=Double.parseDouble(amount.getText());
        if(amount_in<=0){
            error_amount.setVisible(true);
        } else{
            error_amount.setVisible(false);
        }
    }

    /**
     * The function transferMoney gets run, when the button "transfer" gets pressed.
     * It checks for correct inputs and loads the choosen account from the sql table.
     * After that it runs the transferMoney function in transferMoney and loads depending on the outcome the transferError or transferSuccessful window
     * @param event
     */
    @FXML
    private void transferMoney(ActionEvent event){
        event.consume();

        //testing the given values
        if(!Account.checkIBAN(Iban_sender.getText())){
            error_Iban_sender.setVisible(true);
            return;
        }

        if(!Account.checkIBAN(Iban_reciever.getText())){
            error_Iban_reciever.setVisible(true);
            return;
        }

        Account account=Account.initializeAccount(Iban_sender.getText(),Main.getUserAccount().getId());
        if(account==null){
            error_Iban_sender.setVisible(true);
            return;
        }
        error_Iban_sender.setVisible(false);
        error_Iban_reciever.setVisible(false);

        //Actual start of a Money transfer
        int error= transferMoney.transferMoney(account,Iban_reciever.getText(),purpose.getText(),Double.parseDouble(amount.getText()));
        if(error==1){
            //the Balance after this transfer would be beneath 0 and this is not allowed by your Type of account
            try {
                Parent mainWindowRoot;
                mainWindowRoot = FXMLLoader.load(transferError.class.getResource("transferError.fxml"));
                Main.getPrimaryStage().setScene(new Scene(mainWindowRoot));
                Main.getPrimaryStage().show();
            } catch (IOException e) {
                System.out.println("An error occured");
                e.printStackTrace();
                Main.showErrorWindow();
            }

        }else{
            try {
                Parent mainWindowRoot;
                mainWindowRoot = FXMLLoader.load(transferSuccessful.class.getResource("transferSuccessful.fxml"));
                Main.getPrimaryStage().setScene(new Scene(mainWindowRoot));
                Main.getPrimaryStage().show();
            } catch (IOException e) {
                System.out.println("An error occured");
                e.printStackTrace();
                Main.showErrorWindow();
            }

        }
    }

}
