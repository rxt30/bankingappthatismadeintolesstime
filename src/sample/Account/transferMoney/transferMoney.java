package sample.Account.transferMoney;

import sample.Account.Account;
import sample.Main;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The class transferMoney is a backend class for transferring Money to another account
 */
public class transferMoney {

    /**
     * The function transferMoney deducts the transferred Money from your account and updates the account entry.
     * After that it looks, whether the recipient is also a member of our account. If so, the balance of the recipient will be updated in his entry to.
     * At the end the transaction will be saved as an entry into the transaction table.
     * If any errors happen during this process, an integer gets returned to declare which error it is:
     * return 0:everything went right
     * return 1: the Balance after this transfer would be beneath 0 and this is not allowed by your Type of account
     * @param account
     * @param amount
     */
    public static int transferMoney(Account account, String recipientIban, String purpose, double amount){

        double currentBalance2= account.getCurrentBalance()-amount;
        if(account.getKontoType()==1&&currentBalance2<0){
            return 1;
        }
        account.setCurrentBalance(currentBalance2);

        //Writing it into sql
        try {
            PreparedStatement balance =Main.getMariadbConnection().prepareStatement("UPDATE account SET currentBalance = ? WHERE iban = ?");
            balance.setDouble(1,account.getCurrentBalance());
            balance.setString(2,account.getIBAN());
            balance.execute();
            balance.close();
        } catch (SQLException throwables) {
            System.out.println("An error occured");
            throwables.printStackTrace();
            Main.showErrorWindow();
        }

        //check whether receiver Iban_reciever is in or Bank to
        try {
            PreparedStatement reciever = Main.getMariadbConnection().prepareStatement("SELECT * FROM account WHERE iban = ?");
            reciever.setString(1, recipientIban);
            ResultSet queryResult = reciever.executeQuery();
            if(queryResult.next()){
                queryResult.first();
                double Balance = queryResult.getDouble("currentBalance");
                Balance=Balance+amount;
                PreparedStatement ps=Main.getMariadbConnection().prepareStatement("UPDATE account SET currentBalance = ? WHERE iban = ?");
                ps.setString(2, recipientIban);
                ps.setDouble(1,Balance);
                ps.execute();
                ps.close();
            }

        } catch (SQLException e) {
            System.out.println("An error occured");
            e.printStackTrace();
            Main.showErrorWindow();
        }

        //Get Iban_reciever from Sender
        String senderIban=account.getIBAN();

        //Write into transaction table
        try {
            PreparedStatement transaction = Main.getMariadbConnection().prepareStatement("INSERT INTO transaction(senderIban, recipientIban, purpose, amount, date) VALUES (?,?,?,?,?)");
            transaction.setString(1,senderIban);
            transaction.setString(2,recipientIban);
            transaction.setString(3,purpose);
            transaction.setDouble(4,amount);
            transaction.setDate(5, Date.valueOf(java.time.LocalDate.now()));
            transaction.execute();
            transaction.close();
        } catch (SQLException throwables) {
            System.out.println("An error occured");
            throwables.printStackTrace();
            Main.showErrorWindow();
        }

        return 0;
    }
}
