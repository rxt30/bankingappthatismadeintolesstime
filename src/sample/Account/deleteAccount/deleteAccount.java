package sample.Account.deleteAccount;

import sample.Account.Account;
import sample.Main;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The class deleteAccount is a backend class for deleting accounts
 */
public class deleteAccount {


    /**
     * The function deleteAccount tests if the balance of the account equals 0 and deletes the account from the table.
     * @param accountId
     */
    public static void deleteAccount(int accountId){
        double Balance=2;

        //testing whether the balance is also 0 in the account table
        try {
            PreparedStatement loginQuery = Main.getMariadbConnection().prepareStatement("SELECT * FROM account WHERE accountId = ?");
            loginQuery.setInt(1, accountId);
            ResultSet queryResult = loginQuery.executeQuery();
            if (queryResult.next()) {
                queryResult.first();
                Balance = queryResult.getDouble("currentBalance");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        if(Balance!=0){
            Main.showErrorWindow();
            return;
        }

        //Deleting the Account in the sql-table
        try {
            PreparedStatement ps= Main.getMariadbConnection().prepareStatement("DELETE FROM account WHERE accountId=?");
            ps.setInt(1, accountId);
            ps.execute();
            ps.close();
        } catch (SQLException throwables) {
            System.out.println("An error occured");
            throwables.printStackTrace();
            Main.showErrorWindow();
        }

    }
}
