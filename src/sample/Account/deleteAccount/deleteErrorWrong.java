package sample.Account.deleteAccount;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import sample.Login.Login;
import sample.Main;
import sample.MainWindow.MainWindow;

import java.io.IOException;
/**
 * The class deleteErrorWrong is the controller for the window "delete ErrorWrong".
 * The windows deleteErrorWrong give the user the feedback, why his account could not get successfully deleted and takes him back to the Login.
 */
public class deleteErrorWrong {

    /**
     * As soon as the button "Back to Login" gets pressed this method gets started and takes the user back to the menu
     * @param event
     */
    @FXML
    private void loadLogin(ActionEvent event){
        try {
            Parent mainWindowRoot;
            Parent root = FXMLLoader.load(Login.class.getResource("login.fxml"));
            Main.getPrimaryStage().setScene(new Scene(root,300,300));
            Main.getPrimaryStage().show();
        } catch (IOException e) {
            System.out.println("An error occured");
            e.printStackTrace();
            Main.showErrorWindow();
        }
    }
}

