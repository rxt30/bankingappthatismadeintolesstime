package sample.Account.deleteAccount;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import sample.Account.Account;
import sample.Login.Login;
import sample.Main;
import sample.MenuBarController.MenuBarController;

import java.awt.*;
import java.io.IOException;

/**
 * The class Controller is the controller for the window "deleteAccount"
 * In this window the user fills in his credentials and the IBAN from the account, that he wants to delete.
 */
public class Controller extends MenuBarController {

    public Text error;
    public Text error_iban;
    public TextField iban;
    public TextField mail;
    public PasswordField password;


    /**
     * The function deleteAccount gets run, when the button "delete Account" gets pressed.
     * It checks for correct inputs, loads the choosen account from the sql table.
     * After that it checks for the balance of the account and runs the deleteAccount function in the deleteAccount class
     * Depending on the outcome it opens one of the error windows or the successful window
     * @param event
     */
    @FXML
    private void deleteAccount(ActionEvent event){
        event.consume();

        //testing the given values
        if(!Account.checkIBAN(iban.getText())){
            error_iban.setVisible(true);
            return;
        }
        error_iban.setVisible(false);

        int customerid=Main.getUserAccount().getId();

        if (Main.getUserAccount().checkLogin(mail.getText(), password.getText())) {
            error.setVisible(false);

            //check whether user is owner of chosen account
            Account account=Account.initializeAccount(iban.getText(),customerid);
            if(account==null){
                try {
                    Parent mainWindowRoot;
                    mainWindowRoot = FXMLLoader.load(deleteError.class.getResource("deleteErrorWrong.fxml"));
                    Main.getPrimaryStage().setScene(new Scene(mainWindowRoot));
                    Main.getPrimaryStage().show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return;
            }

            //Successful verification and test for the current amount
            if(account.getCurrentBalance()==0) {

                deleteAccount.deleteAccount(account.getAccountId());

                //Load successful Window
                try {
                    Parent mainWindowRoot;
                    mainWindowRoot = FXMLLoader.load(deleteSuccessful.class.getResource("deleteSuccessful.fxml"));
                    Main.getPrimaryStage().setScene(new Scene(mainWindowRoot));
                    Main.getPrimaryStage().show();
                } catch (IOException e) {
                    System.out.println("An error occured");
                    e.printStackTrace();
                    Main.showErrorWindow();
                }
            } else if(account.getCurrentBalance()<0){
                try {
                    Parent mainWindowRoot;
                    mainWindowRoot = FXMLLoader.load(deleteError.class.getResource("deleteErrorNegative.fxml"));
                    Main.getPrimaryStage().setScene(new Scene(mainWindowRoot));
                    Main.getPrimaryStage().show();
                } catch (IOException e) {
                    System.out.println("An error occured");
                    e.printStackTrace();
                    Main.showErrorWindow();
                }
            } else{
                try {
                    Parent mainWindowRoot;
                    mainWindowRoot = FXMLLoader.load(deleteError.class.getResource("deleteErrorPositive.fxml"));
                    Main.getPrimaryStage().setScene(new Scene(mainWindowRoot));
                    Main.getPrimaryStage().show();
                } catch (IOException e) {
                    System.out.println("An error occured");
                    e.printStackTrace();
                    Main.showErrorWindow();
                }
            }
        }else{
            error.setVisible(true);
        }
    }
}
