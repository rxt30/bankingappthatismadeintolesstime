package sample.Account;

import java.math.BigInteger;
import java.sql.*;

import sample.Main;

/**
 * The class Account for new bank account objects.
 */
public class Account {

    /**
     * Attributes of an account:
     * customerId: if from the customer, who owens this account
     * bic: the bis for this account
     * IBAN: the IBAN of the account
     * blz: The blz
     * accountId: the number of the account
     * kontoType: declares the type of account. 0 is invalid, 1 is an "Girokonto", 2 is an "Debitkonto"
     * currentBalance: Balance of the account
     */
    //Attributes
    private int customerId;
    private String bic;
    private String IBAN;
    private int blz;
    private int accountId;
    private int kontoType;
    private double currentBalance;

    /**
     * Constructor for a new account
     */
    public Account(){}

    /**
     * Gets customer id.
     * @return the customer id
     */
    //Getter and Setter
    public int getCustomerId() {
        return customerId;
    }

    /**
     * Gets account id.
     * @return the account id
     */
    public int getAccountId() {
        return accountId;
    }

    /**
     * Gets bic.
     * @return the bic
     */
    public String getBic() {
        return bic;
    }

    /**
     * Gets iban.
     * @return the iban
     */
    public String getIBAN() {
        return IBAN;
    }

    /**
     * Gets blz.
     * @return the blz
     */
    public int getBlz() {
        return blz;
    }

    /**
     * Gets konto type.
     * @return the konto type
     */
    public int getKontoType() {
        return kontoType;
    }

    /**
     * Gets current balance.
     * @return the current balance
     */
    public double getCurrentBalance() {
        return currentBalance;
    }

    /**
     * Sets current balance.
     * @param currentBalance the current balance
     */
    public void setCurrentBalance(double currentBalance) {
       this.currentBalance = currentBalance;
    }

    /**
     * Checks whether the iban is valid or not
     * @param IBAN the iban
     * @return the boolean
     */
    public static boolean checkIBAN(String IBAN){
        //Simply check if Iban_reciever is correct by creating the a Iban_reciever from the given BLZ and accountId
        try {
            int blz = Integer.parseInt(IBAN.substring(4, 12));
            int accountnr = Integer.parseInt(IBAN.substring(12));
            if (IBAN.equals(createIBAN(blz, accountnr))) {
                return true;
            }
            return false;
        }
        catch(Exception error){
            return false;
        }
    }

    /**
     * Generates new IBAN from blz and accountId
     * @param blz       the blz
     * @param accountId the account id(starting zeros should be removed from accountid by user and starting zeros are never given by program)
     * @return the string
     */
    public static String createIBAN(int blz, int accountId){
        //For more info how to calculate a Iban_reciever see www.iban-rechner.de/calculation.html
        String iban = Integer.toString(blz) + ("0000000000" + Integer.toString(accountId)).substring(Integer.toString(accountId).length());
        int checkNumber = new BigInteger(iban + "131400").mod(BigInteger.valueOf(97)).intValue();
        checkNumber = 98 - checkNumber;
        iban = "DE" + ("00" + Integer.toString(checkNumber)).substring(Integer.toString(checkNumber).length()) + iban;
        return iban;
    }

    /**
     * The method initializeAccount creates new Accounts.
     * Then it fills the account with the data from a search by the iban in the account table.
     * After that it checks, whether the given customerId corresponds to the one in the entry.
     * When so, the method returns the account, when not, it returns "null"
     * @param IBAN       the iban
     * @param customerId the customer id
     * @return the account
     */
    public static Account initializeAccount(String IBAN, int customerId){
        // create new Account
        Account account=new Account();

        //read data from the sql table
        try {
            PreparedStatement loginQuery = Main.getMariadbConnection().prepareStatement("SELECT * FROM account WHERE iban = ?");
            loginQuery.setString(1, IBAN);
            ResultSet queryResult = loginQuery.executeQuery();
            if(!queryResult.next()){
                //this account das not exist yet
            }
            queryResult.first();
            //create new object account, fill and return it

            account.fillAccount(queryResult);
            queryResult.close();
            if(account.customerId == customerId){
                return account;
            }
            return null;
        } catch (SQLException e) {
            System.out.println("An error occured");
            e.printStackTrace();
            Main.showErrorWindow();
        }

        return null;
    }

    /**
     * The method fillAccount takes a queryResult and fills the Account with data from this queryResult
     * @param queryResult the query result
     */
    public void fillAccount(ResultSet queryResult){
        try {
            customerId = queryResult.getInt("customerid");
            bic = queryResult.getString("bic");
            IBAN = queryResult.getString("iban");
            blz = queryResult.getInt("blz");
            accountId = queryResult.getInt("accountId");
            kontoType = queryResult.getInt("kontoType");
            currentBalance = queryResult.getDouble("currentBalance");
        } catch (SQLException throwables) {
            System.out.println("An error occured");
            throwables.printStackTrace();
            Main.showErrorWindow();
        }

    }


}
