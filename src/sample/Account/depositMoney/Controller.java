package sample.Account.depositMoney;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;
import sample.Account.Account;
import sample.Login.Login;
import sample.Main;
import sample.MenuBarController.MenuBarController;

import java.io.IOException;
/**
 * The class Controller is the controller for the window "depositMoney"
 * In this window the user fills in the IBAN for his account and the amount for the deposit.
 */

public class Controller extends MenuBarController{
    public TextField amount;
    public TextField iban;
    public Text error_amount;
    public Text error_iban;

    /**
     * The function testAmount gets run, when an action occurs in the field amount.
     * It checks for the correct input and sets the visibility for the error_amount text
     * @param event
     */
    @FXML
    private void testAmount(ActionEvent event){
        event.consume();
        double amount_in=Double.parseDouble(amount.getText());
        if(amount_in<=0){
            error_amount.setVisible(true);
        } else{
            error_amount.setVisible(false);
        }
    }

    /**
     * The function depositMoney gets run, when the button "deposit" gets pressed.
     * It checks for correct inputs and loads the choosen account from the sql table.
     * After that it runs the depositMoney function in depositMoney and loads the depositSuccessful window
     * @param event
     */

    @FXML
    private void depositMoney(ActionEvent event){
        event.consume();
        //testing the given values
        if(!Account.checkIBAN(iban.getText())){
            error_iban.setVisible(true);
            return;
        }

        Account account=Account.initializeAccount(iban.getText(),Main.getUserAccount().getId());
        if(account==null){
            error_iban.setVisible(true);
            return;
        }
        error_iban.setVisible(false);

        double amount_in=Double.parseDouble(amount.getText());
        if(amount_in<=0){
            error_amount.setVisible(true);
            return;
        }
        error_amount.setVisible(false);

        depositMoney.deposit(account,Double.parseDouble(amount.getText()));

        try {
            Parent mainWindowRoot;
            mainWindowRoot = FXMLLoader.load(depositSuccessful.class.getResource("depositSuccessful.fxml"));
            Main.getPrimaryStage().setScene(new Scene(mainWindowRoot));
            Main.getPrimaryStage().show();
        } catch (IOException e) {
            System.out.println("An error occured");
            e.printStackTrace();
            Main.showErrorWindow();
        }
    }

}
