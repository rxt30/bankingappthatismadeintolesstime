package sample.Account.depositMoney;

import sample.Account.Account;
import sample.Main;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * The class depositMoney is a backend class for depositing money to your account
 */

public class depositMoney {

    /**
     * The function depositMoney adds the amount it gets given to the account and updates the account entry, as well as an entry called "ATM" into the transaction table.
     * @param account
     * @param amount
     */

    public static void deposit(Account account, double amount){

        String purpose="Deposit";
        double currentBalance2= account.getCurrentBalance()+amount;
        account.setCurrentBalance(currentBalance2);

        //Writing it into sql
        try {
            PreparedStatement balance =Main.getMariadbConnection().prepareStatement("UPDATE account SET currentBalance = ? WHERE iban = ?");
            balance.setDouble(1,account.getCurrentBalance());
            balance.setString(2,account.getIBAN());
            balance.execute();
            balance.close();
        } catch (SQLException throwables) {
            System.out.println("An error occured");
            throwables.printStackTrace();
            Main.showErrorWindow();
        }

        //Get Iban_reciever from Sender
        String senderIban=account.getIBAN();

        //Write into transaction table
        try {
            PreparedStatement transaction = Main.getMariadbConnection().prepareStatement("INSERT INTO transaction(senderIban, recipientIban, purpose, amount, date) VALUES (?,?,?,?,?)");
            transaction.setString(1,"ATM");
            transaction.setString(2,senderIban);
            transaction.setString(3,purpose);
            transaction.setDouble(4,amount);
            transaction.setDate(5, Date.valueOf(java.time.LocalDate.now()));
            transaction.execute();
            transaction.close();
        } catch (SQLException throwables) {
            System.out.println("An error occured");
            throwables.printStackTrace();
            Main.showErrorWindow();
        }

    }
}
