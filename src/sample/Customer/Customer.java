package sample.Customer;

import sample.Main;

import javax.xml.transform.Result;
import java.sql.Connection;
import java.sql.*;

/**
 * Has all the data for a single customer
 */
public class Customer {
    private int id;
    private String firstName;
    private String lastName;
    private String adress;
    private String city;
    private String mail;
    private String mobileNumber;
    private String birthday;
    private int isAdmin;

    //Getter
    public int getId(){return id;}
    public String getFirstName(){return firstName;}
    public String getLastName(){return lastName;}
    public String getAdress(){return adress;}
    public String getCity(){return city;}
    public String getMail(){return mail;}
    public String getMobileNumber(){return mobileNumber;}
    public String getBirthday(){return birthday;}
    public int isAdmin() {return isAdmin;}


    /**
     * Check if there is a Customer with the given Mail and Password
     * If true fills the Attributes with the Data from the Database
     * If there is no account returns false
     * @param mail Mail from the Customer
     * @param password Password from the Customer
     * @return True if Customer exists and the Attributes could be filled
     */
    public boolean checkLogin(String mail,String password){
        try {
            PreparedStatement loginQuery = Main.getMariadbConnection().prepareStatement("SELECT * FROM customer WHERE mail = ? AND password = ?");
            loginQuery.setString(1,mail);
            loginQuery.setString(2,password);
            ResultSet queryResult = loginQuery.executeQuery();
            loginQuery.close();
            if(!queryResult.next()){
                return false;
            }
            queryResult.first();
            fillCustomer(queryResult);
            return true;
        }
        catch(SQLException error){
            System.out.println("A error occured");
            error.printStackTrace();
            Main.showErrorWindow();
            return false;
        }
        }

    /**
     * Fills the attributes from the given ResultSet
     * @param queryResult Data read from the Database
     */
    public void fillCustomer(ResultSet queryResult){
        try {
            id = queryResult.getInt("id");
            firstName = queryResult.getString("firstName");
            lastName = queryResult.getString("lastName");
            adress = queryResult.getString("adress");
            city = queryResult.getString("city");
            mail = queryResult.getString("mail");
            birthday = queryResult.getString("birthday");
            mobileNumber = queryResult.getString("mobileNumber");
            isAdmin = queryResult.getInt("isAdmin");
        }
        catch(SQLException error){
            System.out.println("A error occured");
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }
}
