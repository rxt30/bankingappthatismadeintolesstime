package sample.TransactionActivity;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import sample.Main;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.concurrent.ExecutionException;

/**
 * Get all the Transactions of  a iban and fills a table with it
 */
public class TransactionActivity {

    /**
    * Fills the TableView with the transactions from the given iban
     * @param outgoingTransactions Decide if show outgoing or incoming Transactions
     * @param dataTable Table to create the Data in
     * @param currentSelectedIban selected iban
     */
    public void fillTableWithData(Boolean outgoingTransactions, TableView dataTable,String currentSelectedIban){
        try {
            ResultSet transactions = getTransactionsResultSet(outgoingTransactions, currentSelectedIban);
            dataTable.getItems().clear();
            dataTable.getColumns().clear();
            makeTableHeader(dataTable,transactions);
            makeTableBody(dataTable,transactions);
            dataTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        }
        catch(Exception error){
            Main.showErrorWindow();
        }
    }

    /**
     * Generates the Header of the Table
     * @param dataTable table to modify
     * @param transactions Results of the SQL-Query for the transactions
     */
    private void makeTableHeader(TableView dataTable, ResultSet transactions){
        TableColumn columnToInsert;
        String currentHeader;
        try {
            for (int i = 1; i <= transactions.getMetaData().getColumnCount(); i++) {
                currentHeader = transactions.getMetaData().getColumnName(i);
                columnToInsert = new TableColumn<Transaction,String>(currentHeader);
                if(i == 1){
                    columnToInsert.setCellValueFactory(new PropertyValueFactory<>("iban"));
                }else{
                    columnToInsert.setCellValueFactory(new PropertyValueFactory<>(currentHeader));
                }
                dataTable.getColumns().add(columnToInsert);
            }
        }
        catch(Exception error){
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }

    /**
     * Fills the table with data
     * @param dataTable Table to fill
     * @param transactions ResultSet with all the found Transactions
     */
    private void makeTableBody(TableView dataTable,ResultSet transactions){
        try {
            transactions.beforeFirst();
            while(transactions.next()){
                dataTable.getItems().add(new Transaction(transactions));
            }
        }
        catch(Exception error){
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }

    /**
     * Creates a ResultSet with all the incoming or outgoing Transactions
     * for the given iban.
     * @param outgoingTransactions Decide if incoming or outgoing Transactions
     * @param iban The given iban to get the Transactions
     * @return ResultSet with the Transactions and the following Columns (recipientIban or senderIban,purpose,amount,date).
     */

    private ResultSet getTransactionsResultSet(Boolean outgoingTransactions,String iban){
        String searchQuery = ",purpose,amount,date FROM transaction ";
        if(outgoingTransactions){
            searchQuery = "SELECT recipientIban" + searchQuery + "WHERE senderIban = ?";
        }else{
            searchQuery = "SELECT senderIban" + searchQuery + "WHERE recipientIban = ?";
        }
        try{
            PreparedStatement transactionsQuery = Main.getMariadbConnection().prepareStatement(searchQuery);
            transactionsQuery.setString(1,iban);
            return transactionsQuery.executeQuery();
        }
        catch(Exception error){
            Main.showErrorWindow();
            return null;
        }
    }
}
