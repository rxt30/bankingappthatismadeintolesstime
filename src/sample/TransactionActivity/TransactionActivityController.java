package sample.TransactionActivity;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Control;
import javafx.scene.control.TableView;
import sample.Main;
import sample.MenuBarController.MenuBarController;

/**
 * Controller of transactionsWindow.fxml
 */
public class TransactionActivityController extends MenuBarController {
    public ComboBox ibanChoice;
    public TableView dataTable;
    private Boolean outgoingTransactions;
    private final TransactionActivity transactionActivity;

    public TransactionActivityController(){
        transactionActivity = new TransactionActivity();
    }

    public void setOutgoingTransactions(Boolean outgoingTransactions) {
        this.outgoingTransactions = outgoingTransactions;
    }

    /*
    Triggered by a change of the iban-Dropdown
    Gets the current iban and passes it to fillTableWithData() to generate the table
     */
    @FXML
    private void showAccountTransactions(Event event){
        if(((ComboBox)event.getSource()).getValue() == null){
            return;
        }
        event.consume();
        String currentSelectedIban = (String) ibanChoice.getValue();
        //dataTable = new TableView();
        transactionActivity.fillTableWithData(outgoingTransactions,dataTable,currentSelectedIban);
    }
}
