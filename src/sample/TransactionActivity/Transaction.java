package sample.TransactionActivity;

import sample.Main;

import java.sql.ResultSet;
import java.util.Date;

/**
 * Used to store a transaction
 */
public class Transaction {
    private String iban;
    private String purpose;
    private double amount;
    private Date date;

    /**
     * Fills all the attributes with data from the given ResutltSet
     *
     * @param TransactionData data for the Object
     */
    public Transaction(ResultSet TransactionData){
        try {
            iban = TransactionData.getString(1);
            purpose = TransactionData.getString(2);
            amount = TransactionData.getDouble(3);
            date = TransactionData.getDate(4);
        }
        catch(Exception error){
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }

    /**
     * Gets iban.
     *
     * @return the iban
     */
//Getter Methods
    public String getIban() {
        return iban;
    }

    /**
     * Gets purpose.
     *
     * @return the purpose
     */
    public String getPurpose() {
        return purpose;
    }

    /**
     * Gets amount.
     *
     * @return the amount
     */
    public String getAmount() {
        return Double.toString(amount);
    }

    public String getDate() {
        return date.toString();
    }
}
