package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.Customer.Customer;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main extends Application {
    private static Stage primaryStage;
    private static Customer userAccount;
    private static Connection mariadbConnection;

    private void setPrimaryStage(Stage stage){
        Main.primaryStage = stage;
    }
    public static Stage getPrimaryStage(){return Main.primaryStage;}
    public static Customer getUserAccount(){return Main.userAccount;}
    public static Connection getMariadbConnection(){return Main.mariadbConnection;}

    /**
     * The default Error-Window to be shown, if something went wrong
     */
    public static void showErrorWindow(){
        try {
            Parent errorRoot = FXMLLoader.load(Main.class.getResource("ErrorWindow/ErrorWindow.fxml"));
            Stage errorWindowStage = new Stage();
            errorWindowStage.setTitle("A error occured");
            errorWindowStage.setScene(new Scene(errorRoot,300,300));
            errorWindowStage.show();
        }
        catch(IOException error){
            error.printStackTrace();
            Main.showErrorWindow();
            }
    }

    /**
     * Creates a new Customer, establish a Connection to the Database and loads the GUI
     * @param primaryStage Stage to load the first Scene in
     */
    @Override
    public void start(Stage primaryStage){
        try {
            Main.userAccount = new Customer();
            Main.mariadbConnection = DriverManager.getConnection("jdbc:mariadb://localhost:3306/banking?user=root&password=root");
            Main.primaryStage = primaryStage;
            Parent root = FXMLLoader.load(getClass().getResource("Login/login.fxml"));
            primaryStage.setTitle("BankingAppThatIsMadeInToLessTime");
            primaryStage.setScene(new Scene(root));
            primaryStage.setMinWidth(355);
            primaryStage.setMinHeight(300);
            primaryStage.show();
        }
        catch(Exception error){
            Main.showErrorWindow();
        }
    }


    public static void main(String[] args){
            launch(args);
    }
}
