package sample.Admin;

import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import sample.Customer.Customer;
import sample.Main;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Generation of the table of adminReportWindows.fxml
 */
public class ReportWindowGeneration {


    /**
     * Reads all Customer from the Database that aren't Admins
     * @return All the Customers that are not Admin
     */
    private ResultSet generateCustomerResultSet(){
        String customerDataQueryStatement = "SELECT id,firstName,lastName,adress,city,mail,mobileNumber,birthday,isAdmin " +
                "FROM customer WHERE isAdmin = 0";
        try {
            PreparedStatement customerDataQuery = Main.getMariadbConnection().prepareStatement(customerDataQueryStatement);
            ResultSet queryData = customerDataQuery.executeQuery();
            customerDataQuery.close();
            return queryData;
        }
        catch(SQLException error){
            System.out.println("Error while reading from the database");
            error.printStackTrace();
            Main.showErrorWindow();
        }
        return null;
    }

    /**
     * Gets all the accounts with the mail for the owners of the accounts
     * @param dataFilter Decides if all Accounts or just Accounts with a negative standing
     * @return All accounts with the mail linked to the owner of the account
     */
    private ResultSet generateAccountsResultSet(String dataFilter){
        String accountDataQueryStatement = "SELECT account.customerId,customer.mail,account.bic,account.iban,account.blz," +
                "account.kontoType,account.currentBalance " +
                "FROM account " +
                "INNER JOIN customer ON account.customerId = customer.id ";
        if(dataFilter.equals("negativeAccounts")){
            accountDataQueryStatement += "WHERE account.currentBalance < 0";
        }
        try{
            PreparedStatement accountsDataQuery = Main.getMariadbConnection().prepareStatement(accountDataQueryStatement);
            ResultSet querySet = accountsDataQuery.executeQuery();
            accountsDataQuery.close();
            return querySet;
        }
        catch(SQLException error){
            System.out.println("A error occured");
            Main.showErrorWindow();
            return null;
        }
    }


    /**
     * Fills the given Table with all the Customers that aren't Admin
     * @param customerTable Table to fill
     */
    public void fillCustomerTable(TableView customerTable) {
        String currentHeader;
        TableColumn columnToInsert;
        Customer currentCustomer;
        ResultSet customerData = generateCustomerResultSet();
        try {
            for (int i = 1; i < customerData.getMetaData().getColumnCount(); i++) {
                currentHeader = customerData.getMetaData().getColumnName(i);
                columnToInsert = new TableColumn<Customer, String>(currentHeader);
                columnToInsert.setCellValueFactory(new PropertyValueFactory<>(currentHeader));
                customerTable.getColumns().add(columnToInsert);
            }
            customerData.beforeFirst();
            while (customerData.next()) {
                currentCustomer = new Customer();
                currentCustomer.fillCustomer(customerData);
                customerTable.getItems().add(currentCustomer);
            }
        }
        catch(SQLException error){
            System.out.println("A error occured");
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }

    /**
     * Fills the given Table with all the Accounts and the mail of the linked Customer
     * @param accountTable Table to fill
     * @param eventId Decide if all Accounts or just Accounts with a negative standing
     */
    public void fillAccountTable(TableView accountTable,String eventId){
        String currentHeader;
        TableColumn columnToInsert;
        CustomersAccount currentAccount;
        ResultSet accountsData = generateAccountsResultSet(eventId);
        try{
            for(int i = 1; i <= accountsData.getMetaData().getColumnCount();i++){
                currentHeader = accountsData.getMetaData().getColumnName(i);
                columnToInsert = new TableColumn<CustomersAccount, String>(currentHeader);
                columnToInsert.setCellValueFactory(new PropertyValueFactory<>(currentHeader));
                accountTable.getColumns().add(columnToInsert);
            }
            accountsData.beforeFirst();
            while(accountsData.next()){
                currentAccount = new CustomersAccount(accountsData);
                accountTable.getItems().add(currentAccount);
            }
        }
        catch(SQLException error){
            System.out.println("A error occured");
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }

    /**
     * Generates a Table according to the given Node id
     * If the node id is customer, it creates a Table with all the customers that aren't Admin
     * Else it creates a Table with all the accounts
     * @param reportWindow Location of the Table
     * @param eventId Id of the Node, that triggered the event
     */
    public void generateDataTable(Stage reportWindow,String eventId){
        TableView customerTable = (TableView) reportWindow.getScene().lookup("#dataTable");
        if(eventId.equals("customers")){
            fillCustomerTable(customerTable);
        }else{
            fillAccountTable(customerTable,eventId);
        }
            customerTable.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
            reportWindow.setWidth(getTableWidth(customerTable));
            reportWindow.setHeight(450);
            customerTable.prefHeightProperty().bind(reportWindow.heightProperty());
            customerTable.prefWidthProperty().bind(reportWindow.widthProperty());
    }

    /**
     * Calculates the width of the table by it's columns
     * @param dataTable Table to get the width from
     * @return Width of the Table
     */
    private double getTableWidth(TableView dataTable){
        double totalWidth = 0;
        for(int i = 0; i < dataTable.getColumns().size(); i++){
            totalWidth += ((TableColumn) dataTable.getColumns().get(i)).getWidth();
        }
        return totalWidth;
    }
}
