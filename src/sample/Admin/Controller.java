package sample.Admin;

import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.event.Event;
import javafx.fxml.FXML;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;

import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import sample.Customer.Customer;
import sample.Main;

import java.io.IOException;
import java.util.function.Predicate;

/**
 * Controller for adminMainWindow.fxml
 */
public class Controller {
    ReportWindowGeneration generationObject;
    DataExport exportObject;
    Scene currentTableScene;

    public Controller(){
        this.generationObject = new ReportWindowGeneration();
        this.exportObject = new DataExport();
    }

    /**
     * Shows a window with the Data selected by the user
     * @param event Triggered event
     */
    @FXML
    private void getData(ActionEvent event){
        try {
            event.consume();
            Stage dataStage = new Stage();
            Parent dataGrid = FXMLLoader.load(getClass().getResource("adminReportWindows.fxml"));
            currentTableScene = new Scene(dataGrid);
            dataStage.setScene(currentTableScene);
            generationObject.generateDataTable(dataStage,((Node) event.getSource()).getId());
            dataStage.show();
        }
        catch(IOException error){
            System.out.println("A error occured");
            error.printStackTrace();
        }
    }

    /**
     * Exportes the data choosen by the user
     * @param event Triggered event
     */
    @FXML
    private void exportData(ActionEvent event){
        event.consume();
        exportObject.getPathToSavefile(((Node) event.getSource()).getId());
    }

}
