package sample.Admin;

import javafx.stage.FileChooser;
import sample.Main;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Exports Data, Data chosen by the user
 */
public class DataExport {
    FileChooser fileChooser;

    public DataExport() {
        fileChooser = new FileChooser();
        fileChooser.setTitle("Choose file or path to save the data:");
    }

    /**
     * Open a Window to let the user choose in which file he want to choose the export
     * @param dataType Which data to write in the file (Customers or Accounts)
     */
    public void getPathToSavefile(String dataType) {
        File saveFile = fileChooser.showSaveDialog(Main.getPrimaryStage());
        if (saveFile != null) {
            writeFile(dataType, saveFile);
        } else {
            getPathToSavefile(dataType);
            return;
        }
    }

    /**
     * Gets the data according to the dataType and writes it into the File
     * @param dataType Which Data to save in the File
     * @param saveFile The File to save the Data in
     */
    public void writeFile(String dataType, File saveFile) {
        try {
            ResultSet dataToWrite;
            int currentColumn;
            FileWriter writeToSaveFile = new FileWriter(saveFile);
            StringBuilder currentLine = new StringBuilder();
            if (dataType.contains("customersExport")) {
                dataToWrite = generateCustomerResultSet();
            } else {
                dataToWrite = generateAccountsResultSet(dataType);
            }
            //Generate table headers
            for (currentColumn = 1; currentColumn < dataToWrite.getMetaData().getColumnCount(); currentColumn++) {
                currentLine.append(dataToWrite.getMetaData().getColumnName(currentColumn));
                currentLine.append(",");
            }
            currentLine.append(dataToWrite.getMetaData().getColumnName(currentColumn));
            currentLine.append("\n");
            writeToSaveFile.write(currentLine.toString());
            //Generate data
            dataToWrite.beforeFirst();
            while (dataToWrite.next()) {
                currentLine = new StringBuilder();
                for (currentColumn = 1; currentColumn < dataToWrite.getMetaData().getColumnCount(); currentColumn++) {
                    currentLine.append(dataToWrite.getString(currentColumn));
                    currentLine.append(",");
                }
                currentLine.append(dataToWrite.getString(currentColumn));
                currentLine.append("\n");
                writeToSaveFile.write(currentLine.toString());
            }
            writeToSaveFile.close();
        } catch (Exception error) {
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }

    /**
     * Get the data for all Customers that aren't admins from the database
     * @return All the Customers. Columns: (id,firstName,lastName,adress,city,mail,mobileNumber,birthday)
     */
    private ResultSet generateCustomerResultSet() {
        String customerDataQueryStatement = "SELECT id,firstName,lastName,adress,city,mail,mobileNumber,birthday " +
                "FROM customer WHERE isAdmin = 0";
        try {
            PreparedStatement customerDataQuery = Main.getMariadbConnection().prepareStatement(customerDataQueryStatement);
            ResultSet queryData = customerDataQuery.executeQuery();
            customerDataQuery.close();
            return queryData;
        } catch (SQLException error) {
            System.out.println("Error while reading from the database");
            error.printStackTrace();
            Main.showErrorWindow();
        }
        return null;
    }

    /**
     * Gets all accounts and the mail to the owner of the account from the database and returns them as a ResultSet.
     * @param dataFilter Decide if all accounts or just accounts with a negative Standing
     * @return All accounts (customerId,mail,bic,iban,blz,kontoType,currentBalance)
     */
    private ResultSet generateAccountsResultSet(String dataFilter) {
        String accountDataQueryStatement = "SELECT account.customerId,customer.mail,account.bic,account.iban,account.blz," +
                "account.kontoType,account.currentBalance " +
                "FROM account " +
                "INNER JOIN customer ON account.customerId = customer.id ";
        if (dataFilter.equals("negativeAccountsExport")) {
            accountDataQueryStatement += "WHERE account.currentBalance < 0";
        }
        try {
            PreparedStatement accountsDataQuery = Main.getMariadbConnection().prepareStatement(accountDataQueryStatement);
            ResultSet querySet = accountsDataQuery.executeQuery();
            accountsDataQuery.close();
            return querySet;
        } catch (SQLException error) {
            System.out.println("A error occured");
            Main.showErrorWindow();
            return null;
        }
    }
}
