package sample.Admin;

import sample.Main;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Holds the data for one account
 */
public class CustomersAccount {
    int customerId;
    String bic;
    String iban;
    String blz;
    String kontoType;
    double currentBalance;
    String mail;

    /**
     * Fills the attributes with the Data from a given ResultSet
     * @param queryResult Data to fill the CustomersAccount with
     */
    public CustomersAccount(ResultSet queryResult){
        try {
            customerId = queryResult.getInt("customerId");
            bic = queryResult.getString("bic");
            iban = queryResult.getString("iban");
            blz = queryResult.getString("blz");
            if(queryResult.getInt("kontoType") == 1){
                kontoType = "Girokonto";
            }else if(queryResult.getInt("kontoType") == 2){
                kontoType = "Debitkonto";
            }else{
                kontoType = "Invaliv Konto";
            }
            currentBalance = queryResult.getDouble("currentBalance");
            mail = queryResult.getString("mail");
        }
        catch(SQLException error){
            System.out.println("A error occured");
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }

    public int getCustomerId() {
        return customerId;
    }

    public double getCurrentBalance() {
        return currentBalance;
    }

    public String getBic() {
        return bic;
    }

    public String getBlz() {
        return blz;
    }

    public String getIban() {
        return iban;
    }

    public String getKontoType() {
        return kontoType;
    }

    public String getMail() {
        return mail;
    }
}
