package sample.CustomerCreation;

import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import sample.Login.Login;
import sample.Main;

import java.io.IOException;

/**
 * Controller for CustomerCreation
 */
public class Controller {

    public GridPane registerPane;
    public Label mailErrorLabel;
    public Label passwordMatchLabel;
    public Button registerButton;
    public TextField passwordField;
    public TextField repeatPasswordField;
    public TextField mailField;
    public DatePicker birthDatePicker;
    private final CustomerCreation newCustomer;

    public Controller(){
        this.newCustomer = new CustomerCreation();
    }

    /**
     * Tries to register a new Customer
     * If the mail already has been registered shows a Error-Message
     * Else loads the login-Scene
     * @param event Triggered Event
     */
    @FXML
    private void registerCustomer(ActionEvent event){
        event.consume();
        boolean mailCheck = newCustomer.checkMail(mailField.getText());
        if(!mailCheck){
            mailErrorLabel.setPrefHeight(16);
            mailErrorLabel.setVisible(true);
            return;
        }
        saveCustomerData();
        backToLogin(event);
    }

    /**
     * Extracts all the Data from the registration-Scene and adds the User to the Database
     */
    private void saveCustomerData(){
        String[] customerData = new String[8];
        for(int i = 1 ;i < 14; i += 2){
            customerData[i/2] = ((TextField) registerPane.getChildren().get(i)).getText();
        }
        customerData[7] = birthDatePicker.getValue().toString();
        newCustomer.addToDatabase(customerData);
    }

    /**
     * Checks if all input fields has been filled and if the passwords equal
     * @param event Triggered event
     */
    @FXML
    private void checkInputFields(Event event) {
        event.consume();
        for (int i = 1; i < 16; i += 2) {
            if (((TextField) registerPane.getChildren().get(i)).getText().equals("") || ((TextField) registerPane.getChildren().get(i)).getText() == null) {
                registerButton.setDisable(true);
                return;
            }
        }
        if (birthDatePicker.getValue() == null || birthDatePicker.getValue().toString().equals("")) {
            return;
        }
        if (checkPasswordFields()) {
            registerButton.setDisable(false);
        }
    }

    /**
     * Checks if password and "repeat password" matches
     * @return True if password matches
     */
    private boolean checkPasswordFields(){
        if(passwordField.getText().equals("") || repeatPasswordField.getText().equals("")){
            return false;
        }
        if(passwordField.getText().equals(repeatPasswordField.getText())){
           passwordMatchLabel.setPrefHeight(0);
            return true;
        }
        passwordMatchLabel.setPrefHeight(16);
        return false;
    }

    /**
     * Loads the login-Scene into the Main-Window
     * @param event Triggered event
     */
    @FXML
    private void backToLogin(ActionEvent event) {
        event.consume();
        try {
            Parent loginWindow = FXMLLoader.load(Login.class.getResource("login.fxml"));
            Main.getPrimaryStage().setScene(new Scene(loginWindow,300,300));
        } catch(IOException error){
            System.out.println("A error occured");
            Main.showErrorWindow();
        }
    }
}
