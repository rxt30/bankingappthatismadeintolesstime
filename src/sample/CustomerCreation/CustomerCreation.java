package sample.CustomerCreation;

import sample.Main;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Used to save a new customer in the database.
 * Also used to check if a mail has already been registered
 */
public class CustomerCreation {

    /**
     * Checks if the Mail is already registered
     * @param mail Mail given by the user
     * @return True if Mail not registered
     */
    public boolean checkMail(String mail) {
        try {
            PreparedStatement mailStatement = Main.getMariadbConnection().prepareStatement("SELECT id FROM customer WHERE mail = ?");
            mailStatement.setString(1,mail);
            ResultSet queryResult = mailStatement.executeQuery();
            mailStatement.close();
            if(!queryResult.next()){
                return true;
            }
            return false;
        } catch (SQLException error) {
            System.out.println("A error occured");
            Main.showErrorWindow();
            return false;
        }
    }

    /**
     * Adds the User to the Database with the given Data
     * @param customerData A Array with all the extracted Data for the user
     */
    public void addToDatabase(String[] customerData){
        try {
            PreparedStatement insertQuery = Main.getMariadbConnection().prepareStatement("INSERT INTO customer " +
                    "(mail,lastName,firstName,adress,city,mobileNumber,password,birthday,isAdmin)" +
                    " VALUES (?,?,?,?,?,?,?,?,0)");
            for(int i = 1; i <= customerData.length;i++){
                insertQuery.setString(i,customerData[i-1]);
            }
            insertQuery.execute();
            insertQuery.close();
        }
        catch(SQLException error){
            System.out.println("A error occured while writing to the database");
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }
}
