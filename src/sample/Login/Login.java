package sample.Login;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableView;
import sample.Admin.DataExport;
import sample.Main;
import sample.MainWindow.MainWindow;

public class Login {

    /**
     * Tries to login a User with the given mail and password
     * If the Login is successful loads the MainWindow or the AdminWindow, depends on the isAdmin-State of the User.
     * If the Login is not successful it shows a Message
     * @param mail Mail-Input from the User
     * @param password Password-Input from the User
     */
    public static void login(String mail,String password) {
        try {
            if (Main.getUserAccount().checkLogin(mail, password)) {
                Parent mainWindowRoot;
                if(Main.getUserAccount().isAdmin() != 0){
                    mainWindowRoot = FXMLLoader.load(DataExport.class.getResource("adminMainWindow.fxml"));
                    ((Label) mainWindowRoot.lookup("#welcomeLabel")).setText("Welcome " + Main.getUserAccount().getFirstName() + " " + Main.getUserAccount().getLastName());
                }else {
                    mainWindowRoot = FXMLLoader.load(MainWindow.class.getResource("mainWindow.fxml"));
                    ((Label) mainWindowRoot.getChildrenUnmodifiable().get(1)).setText("Welcome " + Main.getUserAccount().getFirstName() + " " + Main.getUserAccount().getLastName());
                    TableView accountsTable = (TableView) mainWindowRoot.getChildrenUnmodifiable().get(2);
                    MainWindow.generateAccountsTable(accountsTable);
                }
                Main.getPrimaryStage().setScene(new Scene(mainWindowRoot));
                Main.getPrimaryStage().show();
            }else{
                ((Label) Main.getPrimaryStage().getScene().lookup("#loginErrorLabel")).setPrefHeight(16);
                Main.getPrimaryStage().setMinWidth(355);
            }
        }
        catch(Exception error){
            System.out.println("A error occured");
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }
}
