package sample.Login;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import sample.CustomerCreation.CustomerCreation;
import sample.Main;
import sample.MenuBarController.MenuBarController;

import java.io.IOException;

/**
 * Controller for login.fxml
 */
public class Controller extends MenuBarController {
    public TextField mailField;
    public TextField passwordField;

    /**
     * Extract the input from the mailField and passwordField and tries to login the user
     * @param event Triggered event
     */
    @FXML
    private void login(ActionEvent event){
        event.consume();
        Login.login(mailField.getText(),passwordField.getText());
    }

    /**
     * Loads the Register-Scene
     * @param event Trigger event
     */
    @FXML
    private void changeToRegister(ActionEvent event){
        try{
        event.consume();
        Parent mainWindowRoot = FXMLLoader.load(CustomerCreation.class.getResource("customerCreation.fxml"));
        Main.getPrimaryStage().setScene(new Scene(mainWindowRoot,300,650));
        Main.getPrimaryStage().show();
    }
        catch(IOException error){
            System.out.println("A error occured");
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }
}
