package sample.MenuBarController;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.layout.HBox;
import sample.Account.Account;
import sample.Login.Login;
import sample.Main;
import sample.CustomerChange.CustomerChange;
import sample.MainWindow.MainWindow;
import sample.TransactionActivity.TransactionActivity;
import sample.TransactionActivity.TransactionActivityController;

import java.io.IOException;
import java.net.URL;

/**
 * Controller of the Menubar, that appears in nearly every scene
 */
public class MenuBarController {

    /**
     * Displays the logIn-Scene
     * @param event Triggered Event
     */
    @FXML
    private void logOut(Event event){
        event.consume();
        try {
            Parent root = FXMLLoader.load(Login.class.getResource("login.fxml"));
            Main.getPrimaryStage().setScene(new Scene(root,300,300));
            Main.getPrimaryStage().show();
        }
        catch(Error | IOException error){
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }

    /**
     * Loads the transactions-Scene
     * Decides by the event-Source id if outgoing or incoming Transactions should be loaded
     * @param event Triggered event
     */
    @FXML
    private void showTransactions(Event event){
        event.consume();
        try {
            FXMLLoader loader = new FXMLLoader(TransactionActivity.class.getResource("transactionsWindow.fxml"));
            Parent root = loader.load();
            if(((MenuItem) event.getSource()).getId().contains("Outgoing")) {
                ((TransactionActivityController) loader.getController()).setOutgoingTransactions(true);
            }else{
                ((TransactionActivityController) loader.getController()).setOutgoingTransactions(false);
            }
            ComboBox ibanDropDown = (ComboBox) ((HBox) root.getChildrenUnmodifiable().get(1)).getChildren().get(1);
            MenuBarDataCreation.generateCustomerIbanDropDown(ibanDropDown);
            showNewLoadedScene(root);
        }
        catch(Exception error){
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }

    /**
     * Loads a new Scene
     * @param event Triggered Event
     */
    @FXML
    private void load(Event event){
        try {
            event.consume();
            Parent root = FXMLLoader.load(getUrlForPackage(((MenuItem) event.getSource()).getId()));
            showNewLoadedScene(root);
        }
        catch(Exception error){
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }

    /**
     * Returns the .fxml to generate a new Scene, decides by the itemId
     * @param itemId Given id of the Node, that triggered the event
     * @return A URL-Object to the selected scene
     */
    private URL getUrlForPackage(String itemId){
        if(itemId.equals("ChangeCustomer")){
            return CustomerChange.class.getResource("ChangeCustomer.fxml");
        }else if(itemId.equals("TransferMoney")){
            return Account.class.getResource("transferMoney/transferMoney.fxml");
        }else if(itemId.equals("NewAccount")){
            return Account.class.getResource("createAccount/createAccount.fxml");
        }else if(itemId.equals("DeleteAccount")){
            return Account.class.getResource("deleteAccount/deleteAccount.fxml");
        }else if(itemId.equals("DepositMoney")){
            return Account.class.getResource("depositMoney/depositMoney.fxml");
        }
        return null;
    }


    /**
     * Displays the new scene in the main Window
     * @param root Parent with the new Scene
     */
    private void showNewLoadedScene(Parent root){
        Main.getPrimaryStage().setScene(new Scene(root));
        Main.getPrimaryStage().show();
    }

    /**
     * Loads the main Menu and fills the Window with the data from the customer
     * @param event Triggered event
     */
    @FXML
    private void loadMainMenu(Event event){
        try {
            Parent mainWindowRoot = FXMLLoader.load(MainWindow.class.getResource("mainWindow.fxml"));
            ((Label) mainWindowRoot.getChildrenUnmodifiable().get(1)).setText("Welcome " + Main.getUserAccount().getFirstName() + " " + Main.getUserAccount().getLastName());
            TableView accountsTable = (TableView) mainWindowRoot.getChildrenUnmodifiable().get(2);
            MainWindow.generateAccountsTable(accountsTable);
            showNewLoadedScene(mainWindowRoot);
        }
        catch(Exception error){
            error.printStackTrace();
            Main.showErrorWindow();
        }
    }
}
