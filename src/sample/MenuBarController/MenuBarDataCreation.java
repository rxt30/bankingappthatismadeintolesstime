package sample.MenuBarController;

import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import sample.Main;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Creates a Dropdown-Menu with all ibans of the current user
 */
public class MenuBarDataCreation {

    /**
     * Fills the given ComboBox with all the ibans of the current Customer
     * @param dropDownMenu ComboBox to fill
     */
    public static void generateCustomerIbanDropDown(ComboBox dropDownMenu){
        ResultSet ibanResultSet = getCustomerIban();
        try {
            ibanResultSet.beforeFirst();
            while (ibanResultSet.next()){
                dropDownMenu.getItems().add(new String(ibanResultSet.getString(1)));
            }
        }
        catch(Exception error){
            error.printStackTrace();
        }
    }

    /**
     * Get all the Ibans for the current Customer
     * @return A ResultSet with all the ibans and the following Columns: (iban)
     */
    private static ResultSet getCustomerIban(){
        try{
            String searchQuery = "SELECT iban from account WHERE customerId = ?";
            PreparedStatement ibanStatement = Main.getMariadbConnection().prepareStatement(searchQuery);
            ibanStatement.setInt(1,Main.getUserAccount().getId());
            return ibanStatement.executeQuery();
        }
        catch(Exception error){
            error.printStackTrace();
            Main.showErrorWindow();
            return null;
        }
    }
}
