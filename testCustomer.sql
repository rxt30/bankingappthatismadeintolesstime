-- MariaDB dump 10.17  Distrib 10.4.13-MariaDB, for Linux (x86_64)
--
-- Host: localhost    Database: banking
-- ------------------------------------------------------
-- Server version	10.4.13-MariaDB-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `account`
--

LOCK TABLES `account` WRITE;
/*!40000 ALTER TABLE `account` DISABLE KEYS */;
INSERT INTO `account` (`customerId`, `bic`, `iban`, `blz`, `kontoType`, `currentBalance`, `accountId`) VALUES (9,'Test','DE78420066600000000003','42006660',2,928,3),(9,'Test','DE51420066600000000004','42006660',1,571,4),(10,'Test','DE24420066600000000005','42006660',2,649,5),(10,'Test','DE94420066600000000006','42006660',1,1679,6),(11,'Test','DE67420066600000000007','42006660',2,2177,7),(11,'Test','DE40420066600000000008','42006660',1,477,8);
/*!40000 ALTER TABLE `account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `customer`
--

LOCK TABLES `customer` WRITE;
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
INSERT INTO `customer` (`id`, `firstName`, `lastName`, `adress`, `city`, `mail`, `mobileNumber`, `password`, `isAdmin`, `birthday`) VALUES (9,'Bernd','Lester','Berndstraße 1','Stromdorf','bernd@lester.com','0123456789','berndLester',0,'2000-06-03'),(10,'Helga','Besta','Billystraße 17','Matlikhausen','helga@besta.com','0987654321','helgaBesta',0,'1997-05-03'),(11,'Nordli','Kallax','Klasenweg 249','Istadbrunnen','nordli@kallax.com','0192837465','nordliKallax',0,'1989-03-03'),(12,'Pappis','Samla','Zum Frakte 50','Slukis','pappis@samla.com','0567483921','pappisSamla',1,'1970-04-05');
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `transaction`
--

LOCK TABLES `transaction` WRITE;
/*!40000 ALTER TABLE `transaction` DISABLE KEYS */;
INSERT INTO `transaction` (`senderIban`, `recipientIban`, `purpose`, `amount`, `date`) VALUES ('ATM','DE78420066600000000003','Deposit',1025.49,'2020-06-20'),('ATM','DE51420066600000000004','Deposit',750,'2020-06-20'),('ATM','DE24420066600000000005','Deposit',536.99,'2020-06-20'),('ATM','DE94420066600000000006','Deposit',1749,'2020-06-20'),('ATM','DE67420066600000000007','Deposit',1999.99,'2020-06-20'),('ATM','DE40420066600000000008','Deposit',420.39,'2020-06-20'),('DE78420066600000000003','DE24420066600000000005','Payment for Malfros',129.99,'2020-06-20'),('DE51420066600000000004','DE67420066600000000007','Return for Nordiska',179,'2020-06-20'),('DE24420066600000000005','DE78420066600000000003','Payment for Skandstuhl',19.54,'2020-06-20'),('DE94420066600000000006','DE40420066600000000008','Gift from Lövas',70,'2020-06-20'),('DE67420066600000000007','DE24420066600000000005','Dinner with Produkt',2.39,'2020-06-20'),('DE40420066600000000008','DE78420066600000000003','Hiking with Komplement',13,'2020-06-20');
/*!40000 ALTER TABLE `transaction` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-20 11:09:06
